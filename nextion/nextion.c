//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_usart.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <color_r5g6b5.h>
#include <delay.h>
#include <uart.h>
#include "nextion.h"


static tNextionIFstate NextionIFstate;
static NextionEventFunc EventFunc = 0;  // ��������� �� �������, ���������� �� ������� ������� 
const uint8_t NextionSuffix[] = {0xFF, 0xFF, 0xFF};
static uint8_t RecvBuff[16];
static uint8_t StrBuff[64];


//==============================================================================
// ������� �������� ������� (� ��������� 3 x 0xFF) � �������
//==============================================================================
int8_t Nextion_Send(uint8_t *pBuff)
{
  uint16_t Len = strlen((char *)pBuff);
  int8_t Result = UART_Send(pBuff, Len);
  if (Result != UART_ERR_OK)
    return Result;
  return UART_Send((uint8_t *)NextionSuffix, 3);
}
//==============================================================================


//==============================================================================
// ������� ����� ������ �� �������
//==============================================================================
int8_t Nextion_RecvCode(uint8_t WaitCode, uint8_t *pRecvBuff)
{
  if (!pRecvBuff)
    return NEXTION_ERR_NO_RX_BUFF;
  
  NextionIFstate.AnswerWaiting = 1;
  NextionIFstate.AnswerCodeWait = WaitCode;
  NextionIFstate.pAnswerBuff = pRecvBuff;
  
  uint32_t TOcntr = NextionTimeOutMs;
  while (TOcntr)
  {
    if (NextionIFstate.AnswerReceived)
    {
      NextionIFstate.AnswerReceived = 0;
      return UART_ERR_OK;
    }
    
    TOcntr--;
    delay_ms(1);
  }

  return NEXTION_ERR_NO_ANSVER;
}
//==============================================================================


//==============================================================================
// ������� �������� ������� � ������� � ��������� ������ � �������� ����� ������
//==============================================================================
int8_t Nextion_SendCommand(uint8_t *pCmd, uint8_t WaitCode, uint8_t *pRecvBuff)
{
  int8_t Result = Nextion_Send(pCmd);
  
  if (Result != UART_ERR_OK)
    return Result;
  
  return Nextion_RecvCode(WaitCode, pRecvBuff);
}
//==============================================================================


//==============================================================================
// �������-���������� ���� ������� �� ������� (���������� �� ����������� ����������!)
//==============================================================================
void Nextion_Rx(uint8_t *pBuff, uint16_t Len)
{
  // �������� ������� ���������
  if (!Len)
    return;
  
  // �������� ��������� � ����������� ���������� �������
  if (
      (NextionIFstate.AnswerWaiting) && 
      ((*pBuff == NextionIFstate.AnswerCodeWait) ||
       (*pBuff <= NEXTION_CMD_CODES_STOP))
     )
  {
    // ���������� ��� ���������� ���������� �������
    memcpy(NextionIFstate.pAnswerBuff, pBuff, Len);
    NextionIFstate.AnswerWaiting = 0;
    NextionIFstate.AnswerReceived = 1;
  }
  
  // �������� ��������� � ������� �������
  if (
      (*pBuff >= NEXTION_EVENT_CODES_START) &&  // ��� �������� ��������� � ��������� ����� �������
      (*pBuff <= NEXTION_EVENT_CODES_STOP) &&   // ��� �������� ��������� � ��������� ����� �������
      (*pBuff != NEXTION_STRING) &&             // �� ������� �� ������� ����������� ������ �� �������� ��������
      (*pBuff != NEXTION_NUMBER)                // �� ������� �� ������� ������������ ����� �� �������� ��������
     )
  {
    if (EventFunc)
      EventFunc(pBuff, Len);
  }
}
//==============================================================================


//==============================================================================
// ������������� ���������� � �������� Nextion
//==============================================================================
void Nextion_Init(USART_TypeDef* USARTx, uint32_t BaudRate, NextionEventFunc Func)
{
  EventFunc = Func;
  UART_Init(USARTx, BaudRate, Nextion_Rx);
}
//==============================================================================


//==============================================================================
// ������� ������ ������� Nextion 
//==============================================================================
int8_t Nextion_Reset(void)
{
  // ���������� �������
  Nextion_SendCommand("rest", NEXTION_CMD_FINISHED, 0);
  // ������� �� ��� ������� �� ��������, ������� ��������� ������ �������������
  return NEXTION_CMD_FINISHED;
}
//==============================================================================


//==============================================================================
// ������� ��������� �������� ���������� ��� �������� ���������� (��������� ��������).
// ������� �� �������� �� ��� �������
//==============================================================================
int16_t Nextion_SetValue_String(char *ValueName, char *Value)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "%s=\"%s\"", ValueName, Value);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, 0);
  // ���������� ��������� ���������� �������
  return NEXTION_CMD_FINISHED;    
}
//==============================================================================


//==============================================================================
// ������� ��������� �������� ���������� ��� �������� ���������� (�������� ��������)
// ����� ������������� �������� ��������� ���������� �� ������� "System Variables List" ������������
// ������� �� �������� �� ��� �������
//==============================================================================
int16_t Nextion_SetValue_Number(char *ValueName, int32_t Value)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "%s=%d", ValueName, Value);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, 0);
  // ���������� ��������� ���������� �������
  return NEXTION_CMD_FINISHED;    
}
//==============================================================================


//==============================================================================
// ������� ������ �������� ���������� ��� �������� ���������� (��������� ��������)
//==============================================================================
int16_t Nextion_GetValue_String(char *ValueName, char *OutString)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "get %s", ValueName);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_STRING, RecvBuff);
  if (Err == UART_ERR_OK)
    strcpy(OutString, (const char *)&RecvBuff[1]);
  // ���������� ��������� ���������� �������
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������ �������� ���������� ��� �������� ���������� (�������� ��������)
//==============================================================================
int16_t Nextion_GetValue_Number(char *ValueName, int32_t *OutValue)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "get %s", ValueName);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_NUMBER, RecvBuff);
  if (Err == UART_ERR_OK)
    memcpy(OutValue, &RecvBuff[1], 4);
  // ���������� ��������� ���������� �������
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������� ������� �������� (�����) ������� �� ��������� Page ID ��� Page Name � ������������ ��������� �
//==============================================================================
int16_t Nextion_SetCurrentPage(char *Page)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "page %s", Page);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������ �� ������� PageID ������� ��������
//==============================================================================
int16_t Nextion_GetCurrentPage(int32_t *OutValue)
{
  // ���������� �������
  int8_t Err = Nextion_SendCommand("sendme", NEXTION_NUMBER, RecvBuff);
  if (Err == UART_ERR_OK)
    memcpy(OutValue, &RecvBuff[1], 4);
  // ���������� ��������� ���������� �������
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������������� ��������� ��������� �� ������� �������� �� Component ID ��� Component Name
//==============================================================================
int16_t Nextion_ComponentRefresh(char *Component)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "ref %s", Component);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ��������� ������� (Event=1) ��� ���������� (Event=0) ���������� �� ������� ��������
//==============================================================================
int16_t Nextion_ComponentClick(char *Component, uint8_t Event)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "click %s,%d", Component, Event);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ��������/��������� ����������� ���������� �� ��� ����� ��� ID
//==============================================================================
int16_t Nextion_ComponentVisibility(char *Component, uint8_t IsVisible)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "vis %s,%d", Component, IsVisible);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ��������/��������� ������� ���������� �� ������������� (touch) �� ��� ����� ��� ID
//==============================================================================
int16_t Nextion_ComponentTouchFunc(char *Component, uint8_t IsVisible)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "tsw %s,%d", Component, IsVisible);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������������� ���������� ������ �������
//==============================================================================
int16_t Nextion_StopRefresh(void)
{
  // ���������� �������
  int8_t Err = Nextion_SendCommand("ref_stop", NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������������ ���������� ������ ������� ����� ���������� ���������
//==============================================================================
int16_t Nextion_StartRefresh(void)
{
  // ���������� �������
  int8_t Err = Nextion_SendCommand("ref_star", NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ��������� ������� � ����� ���������� ���-������
//==============================================================================
int16_t Nextion_TouchCalibration(void)
{
  // ���������� �������
  int8_t Err = Nextion_SendCommand("touch_j", NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������������� ���������� ������, ������������ �� UART (������� ������������ � �����)
//==============================================================================
int16_t Nextion_StopCommandExec(void)
{
  // ���������� �������
  int8_t Err = Nextion_SendCommand("com_stop", NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������������ ���������� ������, ������������ �� UART
//==============================================================================
int16_t Nextion_StartCommandExec(void)
{
  // ���������� �������
  int8_t Err = Nextion_SendCommand("ref_star", NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������� ����� ������ �������
//==============================================================================
int16_t Nextion_ClearCommandBuff(void)
{
  // ���������� �������
  int8_t Err = Nextion_SendCommand("code_c", NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ����� �������� �������� ��� ���������� ��������� ����� �������
//==============================================================================
int16_t Nextion_SetRandomInterval(uint32_t Min, uint32_t Max)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "ranset %d,%d", Min, Max);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������� ��������� ����� � ��������� ����������� ������ (WaveForm)
// ������� �� �������� �� ��� �������
//==============================================================================
int16_t Nextion_Wave_AddPoint(uint8_t ComponentID, uint8_t Channel, uint8_t Value)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "add %d,%d,%d", ComponentID, Channel, Value);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, 0);
  return NEXTION_CMD_FINISHED;    
}
//==============================================================================


//==============================================================================
// ������� ������� ����� ����� � ��������� ����������� ������ (WaveForm)
//==============================================================================
int16_t Nextion_Wave_AddPoints(uint8_t ComponentID, uint8_t Channel, uint8_t *pArray, uint16_t PointsNum)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "add %d,%d,%d", ComponentID, Channel, PointsNum);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_DATA_TRANSPARENT_TX_RDY, RecvBuff);
  if (Err == UART_ERR_OK)       // ������� ���������� ���������� ��������� ������
  {
    // �������� �������� ����� ������� (������ �����)
    UART_Send(pArray, PointsNum);
    return NEXTION_CMD_FINISHED;    
  }
  return Err;
}
//==============================================================================


//==============================================================================
// ������� ������� ���� (Channel=0..3) ��� ��� ������ (Channel=255) ���������� ����������� ������ (WaveForm)
//==============================================================================
int16_t Nextion_Wave_Clear(uint8_t ComponentID, uint8_t Channel)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "cle %d,%d", ComponentID, Channel);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������������� ����� GPIO. Mode: 
//  0 - pull up input mode
//  1 - input binding mode
//  2 - push pull output mode
//  3 - PWM output mode
//  4 - open drain output mode
// BindingName - ��� (��� ID) ���������� � ��������� ����, � �������� ������������� ��������� GPIO
//==============================================================================
int16_t Nextion_GPIO_Config(uint8_t GPIO_Num, uint8_t Mode, char *BindingName)
{
  // ��������� ������ ������� ��� ��������
  sprintf((char *)StrBuff, "cfgpio %d,%d,%s", GPIO_Num, Mode, BindingName);
  // ���������� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� �������� ������� ������
//==============================================================================
int16_t Nextion_ClearScreen(tColor_R5G6B5 Color)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "cls %d", Color.ColorValue);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������� � ��������� ���������� �������� � �������� PictureID
//==============================================================================
int16_t Nextion_DisplayPicture(uint16_t X, uint16_t Y, uint8_t PictureID)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "pic %d,%d,%d", X, Y, PictureID);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������� � ��������� ���������� ����� �������� ���������� ������� � �������� PictureID.
// �������� �������� ������ ���� �������������.
//==============================================================================
int16_t Nextion_DisplayCropPicture(uint16_t X, uint16_t Y, uint16_t W, uint16_t H, uint8_t PictureID)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "picq %d,%d,%d,%d,%d", X, Y, W, H, PictureID);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������� � ��������� ���������� ����� �������� ���������� ������� � �������� PictureID.
// X, Y - ��������� ���������� �������.
// W, H - ������� ����������� ����
// X0, Y0 - ��������� ���������� ��������
// �������� �������� ������ ���� �������������.
//==============================================================================
int16_t Nextion_DisplayAdvancedCropPicture(uint16_t X, uint16_t Y, 
                                          uint16_t W, uint16_t H, 
                                          uint16_t X0, uint16_t Y0,
                                          uint8_t PictureID)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "xpic %d,%d,%d,%d,%d,%d,%d", X, Y, W, H, X0, Y0, PictureID);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������� ����� � ��������� ����������� �� ������� � ���������� �����
//==============================================================================
int16_t Nextion_PrintString_SolidBackColor(uint16_t X, uint16_t Y, uint16_t W, uint16_t H, 
                           uint8_t FontID, tColor_R5G6B5 FontColor, tColor_R5G6B5 BackColor,
                           eXcenter Xcenter, eYcenter Ycenter, char *Str)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "xstr %d,%d,%d,%d,%d,%d,%d,%d,%d,1,\"%s\"", 
                       X, Y, W, H, FontID, FontColor.ColorValue, BackColor.ColorValue, Xcenter, Ycenter, Str);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������� ����� � ��������� ����������� �� ������� � ��������� �� ���� ��� ��� ����
//==============================================================================
int16_t Nextion_PrintString_ImageBack(uint16_t X, uint16_t Y, uint16_t W, uint16_t H, 
                           uint8_t FontID, tColor_R5G6B5 FontColor, eBackMode BackMode, uint8_t PictureID,
                           eXcenter Xcenter, eYcenter Ycenter, char *Str)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "xstr %d,%d,%d,%d,%d,%d,%d,%d,%d,%d,\"%s\"", 
                       X, Y, W, H, FontID, FontColor.ColorValue, PictureID, Xcenter, Ycenter, BackMode, Str);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������ �� ������� ������ �����
//==============================================================================
int16_t Nextion_DrawLine(uint16_t X, uint16_t Y, uint16_t X2, uint16_t Y2, tColor_R5G6B5 Color)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "line %d,%d,%d,%d,%d", X, Y, X2, Y2, Color.ColorValue);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������ ������������� ������������� �� �������
//==============================================================================
int16_t Nextion_DrawRectangle(uint16_t X, uint16_t Y, uint16_t X2, uint16_t Y2, tColor_R5G6B5 Color)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "draw %d,%d,%d,%d,%d", X, Y, X2, Y2, Color.ColorValue);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ����������� ������������� ������� �� �������
//==============================================================================
int16_t Nextion_FillRectangle(uint16_t X, uint16_t Y, uint16_t W, uint16_t H, tColor_R5G6B5 Color)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "fill %d,%d,%d,%d,%d", X, Y, W, H, Color.ColorValue);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ������ ���������� �� �������
//==============================================================================
int16_t Nextion_DrawCircle(uint16_t X, uint16_t Y, uint8_t Radius, tColor_R5G6B5 Color)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "cir %d,%d,%d,%d", X, Y, Radius, Color.ColorValue);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


//==============================================================================
// ������� ����������� ������� ������� �� �������
//==============================================================================
int16_t Nextion_FillCircle(uint16_t X, uint16_t Y, uint8_t Radius, tColor_R5G6B5 Color)
{
  // ��������� ������ ������� ��� ��������
  int8_t Len = sprintf((char *)StrBuff, "cirs %d,%d,%d,%d", X, Y, Radius, Color.ColorValue);
  // �������� ��������� ����� �������
  int8_t Err = Nextion_SendCommand(StrBuff, NEXTION_CMD_FINISHED, RecvBuff);
  return (Err == NEXTION_CMD_FINISHED)? RecvBuff[0]: Err;    
}
//==============================================================================


