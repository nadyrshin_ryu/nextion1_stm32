//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _NEXTION_H
#define _NEXTION_H

#include "../types.h"
#include <color_r5g6b5.h>


#define NextionRxBuffSize       8
#define NextionTimeOutMs        20      // ������� ������� ������ �� ������� � ��


// ������������ ���� ������
#define NEXTION_ERR_NO_ANSVER   (-3)    // ��� ������ - ���� ����-��� �������������� �� �������
#define NEXTION_ERR_NO_RX_BUFF  (-4)    // ��� ������ - �� ������� ��������� �� ������� �����

// �������� �����, ���������� ������ ������ � ����� �� �������
#define NEXTION_CMD_CODES_START         0x00
#define NEXTION_CMD_CODES_STOP          0x1F
// ����, ���������� ������ ������ � ����� �� �������
#define NEXTION_INVALID_CMD             0x00    // �� ���������� �������
#define NEXTION_CMD_FINISHED            0x01    // ������� ��������� �������
#define NEXTION_INVALID_COMPONENT_ID    0x02    // ������ � ID ����������
#define NEXTION_INVALID_PAGE_ID         0x03    // ������ � ID ��������
#define NEXTION_INVALID_PICTURE_ID      0x04    // ������ � ID ��������
#define NEXTION_INVALID_FONT_ID         0x05    // ������ � ID ������
#define NEXTION_INVALID_BAUD            0x11    // ������������ �������� ����� UART
#define NEXTION_CURVEID_OR_CHANNEL_ERR  0x12    // ������ � � ������ ���������� ����������� ������
#define NEXTION_INVALID_VARIABLE        0x1A    // ������ � ����� ����������
#define NEXTION_INVALID_OPERATION       0x1B    // ������ � �������� (����������) ��� ������������� ���� ������
#define NEXTION_FAILED_TO_ASSIGN        0x1C    // ������ - ���������� ���������
#define NEXTION_OPERATE_PERFROM_FAILED	0x1D    // ������ - �� ������� ��������� (�� ��� ��� �� ������)
#define NEXTION_PARAMETER_QUANTITY_ERR  0x1E    // ������ - �������� ���-�� ���������� �������
#define NEXTION_IO_OPERATE_FAILED       0x1F    // ������ �������� �����-������

// �������� �����, ���������� ������ ������ ��� �������� ��������� � �������� �������
#define NEXTION_EVENT_CODES_START       0x65
#define NEXTION_EVENT_CODES_STOP        0xFE
// ����, ���������� ������ ������ ��� �������� ��������� � �������� �������
#define NEXTION_EVENT_TOUCH             0x65    // ������� �������������/���������� ���������� �� ������
#define NEXTION_CURRENT_PAGE_ID         0x66    // ������� �����
#define NEXTION_EVENT_POSITION          0x67    // ������� �������������/���������� ������
#define NEXTION_EVENT_SLEEP_POSITION    0x68    // ������� �������������/���������� ������ ���� ������� ����
#define NEXTION_STRING                  0x70    // ������ ��������� ��������, ����������� �� �������� ��� �� UART
#define NEXTION_NUMBER                  0x71    // ������ �������� ��������, ����������� �� �������� ��� �� UART
#define NEXTION_DEVICE_AUTO_SLEEP_MODE  0x86    // ������� ��������� ������� � ����� ��� (�� �� ������� sleep = 1)
#define NEXTION_DEVICE_AUTO_WAKEUP      0x87    // ������� ��������� ��������� (�� �� ������� sleep=0)
#define NEXTION_EVENT_LAUNCHED          0x88    // ������� ��������� � �����������������
#define NEXTION_EVENT_UPGRADED          0x89    // ������� �������� ���������� � SD-����� 
#define NEXTION_DATA_TRANSPARENT_TX_RDY 0xFE    // ������� ������������ ���������� � ����� ������ (��� ������� addt, ��������)


// ��� ������ ������������� ��������� ������������ �� ����������� � ���������
typedef enum {xLeft = 0, xCenter = 1, xRight = 2} eXcenter;
typedef enum {yTop = 0, yCenter = 1, yBottom = 2} eYcenter;
// �������� ���������� ����
typedef enum {CropImage = 0, Image = 2, NoBackcolor = 3} eBackMode;


typedef struct
{
  uint8_t *pAnswerBuff;
  uint8_t AnswerCodeWait;
  uint8_t AnswerWaiting         :1;     // ��� �������� ������ �� �������
  uint8_t AnswerReceived        :1;     // ����� �� ������� �������
}
tNextionIFstate;


// ��� ��������� �� ������� ��������� ��������� ������
typedef void (*NextionEventFunc)(uint8_t *pBuff, uint16_t Len);



// ������������� ���������� � �������� Nextion
void Nextion_Init(USART_TypeDef* USARTx, uint32_t BaudRate, NextionEventFunc Func);
// ������� �������� ������� � ������� � ��������� ������ � �������� ����� ������
int8_t Nextion_SendCommand(uint8_t *pCmd, uint8_t WaitCode, uint8_t *pRecvBuff);

// ������� ������ ������� Nextion
int8_t Nextion_Reset(void);
// ������� ��������� �������� ���������� ��� �������� ���������� (��������� ��������)
int16_t Nextion_SetValue_String(char *ValueName, char *Value);
// ������� ��������� �������� ���������� ��� �������� ���������� (�������� ��������)
// ����� ������������� �������� ��������� ���������� �� ������� "System Variables List" ������������
int16_t Nextion_SetValue_Number(char *ValueName, int32_t Value);
// ������� ������ �������� ���������� ��� �������� ���������� (��������� ��������)
int16_t Nextion_GetValue_String(char *ValueName, char *OutString);
// ������� ������ �������� ���������� ��� �������� ���������� (�������� ��������)
int16_t Nextion_GetValue_Number(char *ValueName, int32_t *OutValue);
// ������� ������� ������� �������� (�����) ������� �� ��������� Page ID ��� Page Name � ������������ ��������� �
int16_t Nextion_SetCurrentPage(char *Page);
// ������� ������ �� ������� PageID ������� ��������
int16_t Nextion_GetCurrentPage(int32_t *OutValue);
// ������� ������������� ��������� ��������� �� ������� �������� �� Component ID ��� Component Name
int16_t Nextion_ComponentRefresh(char *Component);
// ������� ��������� ������� (Event=1) ��� ���������� (Event=0) ���������� �� ������� ��������
int16_t Nextion_ComponentClick(char *Component, uint8_t Event);
// ������� ��������/��������� ����������� ���������� �� ��� ����� ��� ID
int16_t Nextion_ComponentVisibility(char *Component, uint8_t IsVisible);
// ������� ��������/��������� ������� ���������� �� ������������� (touch) �� ��� ����� ��� ID
int16_t Nextion_ComponentTouchFunc(char *Component, uint8_t IsVisible);
// ������� ������������� ���������� ������ �������
int16_t Nextion_StopRefresh(void);
// ������� ������������ ���������� ������ ������� ����� ���������� ���������
int16_t Nextion_StartRefresh(void);
// ������� ��������� ������� � ����� ���������� ���-������
int16_t Nextion_TouchCalibration(void);
// ������� ������������� ���������� ������, ������������ �� UART (������� ������������ � �����)
int16_t Nextion_StopCommandExec(void);
// ������� ������������ ���������� ������, ������������ �� UART
int16_t Nextion_StartCommandExec(void);
// ������� ������� ����� ������ �������
int16_t Nextion_ClearCommandBuff(void);
// ������� ����� �������� �������� ��� ���������� ��������� ����� �������
int16_t Nextion_SetRandomInterval(uint32_t Min, uint32_t Max);
// ������� ������� ��������� ����� � ��������� ����������� ������ (WaveForm)
int16_t Nextion_Wave_AddPoint(uint8_t ComponentID, uint8_t Channel, uint8_t Value);
// ������� ������� ����� ����� � ��������� ����������� ������ (WaveForm)
int16_t Nextion_Wave_AddPoints(uint8_t ComponentID, uint8_t Channel, uint8_t *pArray, uint16_t PointsNum);
// ������� ������� ���� (Channel=0..3) ��� ��� ������ (Channel=255) ���������� ����������� ������ (WaveForm)
int16_t Nextion_Wave_Clear(uint8_t ComponentID, uint8_t Channel);
// ������� �������� ������� ������
int16_t Nextion_ClearScreen(tColor_R5G6B5 Color);
// ������� ������� � ��������� ���������� �������� � �������� PictureID
int16_t Nextion_DisplayPicture(uint16_t X, uint16_t Y, uint8_t PictureID);
// ������� ������� � ��������� ���������� ����� �������� ���������� ������� � �������� PictureID.
// �������� �������� ������ ���� �������������.
int16_t Nextion_DisplayCropPicture(uint16_t X, uint16_t Y, uint16_t W, uint16_t H, uint8_t PictureID);
// ������� ������� � ��������� ���������� ����� �������� ���������� ������� � �������� PictureID.
// X, Y - ��������� ���������� �������; W, H - ������� ����������� ����; X0, Y0 - ��������� ���������� ��������
// �������� �������� ������ ���� �������������.
int16_t Nextion_DisplayAdvancedCropPicture(uint16_t X, uint16_t Y, uint16_t W, uint16_t H, 
                                          uint16_t X0, uint16_t Y0, uint8_t PictureID);
// ������� ������� ����� � ��������� ����������� �� ������� � ���������� �����
int16_t Nextion_PrintString_SolidBackColor(uint16_t X, uint16_t Y, uint16_t W, uint16_t H, 
                                          uint8_t FontID, tColor_R5G6B5 FontColor, tColor_R5G6B5 BackColor,
                                          eXcenter Xcenter, eYcenter Ycenter, char *Str);
// ������� ������� ����� � ��������� ����������� �� ������� � ��������� �� ���� ��� ��� ����
int16_t Nextion_PrintString_ImageBack(uint16_t X, uint16_t Y, uint16_t W, uint16_t H, 
                                     uint8_t FontID, tColor_R5G6B5 FontColor, eBackMode BackMode, uint8_t PictureID,
                                     eXcenter Xcenter, eYcenter Ycenter, char *Str);
// ������� ������ �� ������� ������ �����
int16_t Nextion_DrawLine(uint16_t X, uint16_t Y, uint16_t X2, uint16_t Y2, tColor_R5G6B5 Color);
// ������� ������ ������������� ������������� �� �������
int16_t Nextion_DrawRectangle(uint16_t X, uint16_t Y, uint16_t X2, uint16_t Y2, tColor_R5G6B5 Color);
// ������� ����������� ������������� ������� �� �������
int16_t Nextion_FillRectangle(uint16_t X, uint16_t Y, uint16_t W, uint16_t H, tColor_R5G6B5 Color);



#endif