//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <nextion.h>
#include <uart.h>
#include <delay.h>
#include "main.h"


int16_t Err;
uint8_t cntr8;
uint32_t cntr32;
int8_t CurrentPageID = 0;
int32_t Val = 0;

const uint8_t SinTable[] = {
128, 134, 141, 147, 153, 160, 166, 172, 178, 184, 189, 195, 200, 205, 210, 215,
220, 224, 228, 232, 236, 239, 242, 245, 247, 249, 251, 253, 254, 255, 255, 255,
255, 255, 255, 254, 253, 251, 249, 247, 244, 242, 238, 235, 231, 228, 223, 219,
214, 210, 205, 199, 194, 188, 183, 177, 171, 165, 159, 152, 146, 140, 133, 127,
121, 114, 108, 102, 95,  89,  83,  77,  71,  66,  60,  55,  50,  45,  40,  35,
31,  27,  23,  20,  16,  13,  11,  8,   6,   4,   3,   2,   1,   0,   0,   0,
0,   0,   0,   1,   2,   4,   5,   7,   9,   12,  15,  18,  21,  25,  29,  33,
38,  42,  47,  52,  58,  63,  69,  74,  80,  86,  92,  98,  105, 111, 117, 124
};


//==============================================================================
// ���������-���������� ������� �� �������. pBuff - ����� � �������� �� ������� 
// (��. Format of Device Return Data - Table 2: other data return format � ������������)
//==============================================================================
void Nex_Event(uint8_t *pBuff, uint16_t Len)
{
  //��������� �������� ��������� ������ ��� ������ ��������� ������� ����������� ����������� ��
  pBuff[Len] = 0x00;
  
  // �������������� �������
  switch (*(pBuff++))
  {
    // ������� �������������/���������� ���������� �� ������
  case NEXTION_EVENT_TOUCH:
    break;
    // ������� �����
  case NEXTION_CURRENT_PAGE_ID:
    CurrentPageID = *pBuff;
    break;
    // ������� �������������/���������� ������
  case NEXTION_EVENT_POSITION:
    break;
    // ������� �������������/���������� ������ ���� ������� ����
  case NEXTION_EVENT_SLEEP_POSITION:
    break;
    // ������� ��������� ������� � ����� ��� (�� �� ������� sleep = 1)
  case NEXTION_DEVICE_AUTO_SLEEP_MODE:
    break;
    // ������� ��������� ��������� (�� �� ������� sleep=0)
  case NEXTION_DEVICE_AUTO_WAKEUP:
    break;
    // ������ ��������� � �����������������
  case NEXTION_EVENT_LAUNCHED:
    break;
    // ������� �������� ���������� � SD-����� 
  case NEXTION_EVENT_UPGRADED:
    break;
  }
}
//==============================================================================


//==============================================================================
//
//==============================================================================
void main()
{
  SystemInit();
  
  Nextion_Init(USART1, 57600, Nex_Event);
  Nextion_Reset();
  
  while (1)
  {
    switch (CurrentPageID)
    {
      // main           �������� �����
    case 0:
      // ���������� �������� ���������� cntr � ��������� n6 � ����� ������� ���� ������
      Err = Nextion_SetValue_Number("n6.val", cntr32++);
      delay_ms(100);
      break;
      // buttons        ����� � ���������� ���������� ������ ����������
    case 1:
      break;
      // waves          ����� � ���������
    case 2:
      Err = Nextion_Wave_AddPoint(1, 0, cntr8);
      Err = Nextion_Wave_AddPoint(1, 1, 255 - cntr8);
      Err = Nextion_Wave_AddPoint(1, 2, SinTable[cntr8 & 0x7F]);
      Err = Nextion_Wave_AddPoint(1, 3, (SinTable[(cntr8 << 1) & 0x7F] >> 1) + 63);
      cntr8++;
      delay_ms(10);
      break;
      // dim_bl         ����� ����������� ������� �������
    case 3:
      // ������ ������� ��������� �������� ������ ������� ������� 
      // ������ ������ h0.txt (�������������� ��������) �������� � ��������� �������
      Err = Nextion_GetValue_Number("h0.val", &Val);
      delay_ms(500);
      break;
      // clock          ����� ��������� �������/����
    case 4:
      break;
    }
  }
}
//==============================================================================
